# Проект PC-интерфейса робота телеприсутствия "Bender", разработанного на Garage48

[Ссылка на описание проекта и процесс разработки](http://lab409.ru/we-rocked-in-kaliningrad-%D0%BE%D1%82%D1%87%D0%B5%D1%82-%D0%BE-garage48/)

##Состав репозитория:

* *README.md* - этот файл;
	
* *StreamViewer.py* - модуль передачи потокового видео с робота на PC;
	
* *client_tcp.py* - модуль TCP-клиента;
	
* *client_webcamera.py* - модуль клиента веб-камеры;
	
* *echotcpserver.py* - эхо-TCP-сервер;
	
* *killalhumans_final.py* - главный модуль;
	
* *killalhumans_research.py* - главный модуль для тестирования фич;

##Использование проекта:

1. Подключаемся к роутеру Bender;
2. Запускаем *killalhumans_final.py*
3. Управляем роботом.
	