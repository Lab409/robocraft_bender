import socket
 

TCP_IP = '127.0.0.1'
TCP_PORT = 5050
BUFFER_SIZE = 1024 # Normally 1024, but we want fast response
COUNT_LISTEN = 5

print "Server is running"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(COUNT_LISTEN)

while True:
    print "Waiting for connections"
    
    conn, addr = s.accept()
    
    print 'Connection established from', addr
    
    while True:
        data = conn.recv(BUFFER_SIZE)
        if not data: 
            print "Connection aborted from", addr
            break
        print 'Received data:', data
        conn.send("Echo =>" + data)  # echo
    conn.close()