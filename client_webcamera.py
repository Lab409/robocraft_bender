#!/usr/bin/env python
# -*- coding: utf-8 -*-
import httplib    #httplib modules for the HTTP interactions
from Tkinter import * #Tkinter modules for Windowing
from PIL import Image, ImageTk #Python Image Libraries, required for displaying jpegs
from time import sleep
import StringIO 		#For converting Stream from the server to IO for Image (PIL)
from StreamViewer import StreamViewer
import pygame
import sys
from pygame.locals import *
import socket		
import time
 
HOST_DRIVE = 'localhost'
PORT_DRIVE = 5050 
#HOST_DRIVE = '192.168.0.200'
#PORT_DRIVE = 2000
HOST_VIDEO = '192.168.0.250'
PORT_VIDEO = 8080
SIZE = 320, 240

H_CENTER = 135
V_CENTER = 90
VEL_STEP = 5
V_MIN = 5
V_MAX = 150
H_MIN = 110
H_MAX = 160

SPEED_1 = "200"
SPEED_2 = "225"
SPEED_3 = "255"  

#Colors
black    = (   0,   0,   0)
white    = ( 255, 255, 255)
green    = (   0, 255,   0)
     
'''Gets the file from the specified
host, port and location/query'''
def get(host,port,query):
     h = httplib.HTTP(host, port)
     h.putrequest('GET', query)
     h.putheader('Host', host)
     h.putheader('User-agent', 'python-httplib')
     h.putheader('Content-type', 'image/jpeg')
     h.endheaders()
     
     (returncode, returnmsg, headers) = h.getreply()
     print "return code:",returncode
     print "return message:",returnmsg
     print "headers:",headers
     if returncode != 200:
         print returncode, returnmsg
         sys.exit()
     
     f = h.getfile()
     return f.read()

'''This is where we show the file on our StreamViewer'''
def exit_proc():
    sock.close()
    sys.exit(0)

def streamfile(tbk, root):
     f = get(HOST_VIDEO,PORT_VIDEO,'/?action=snapshot')
     img=Image.open(StringIO.StringIO(f)) #convert to jpeg object from the stream
     imagetk = ImageTk.PhotoImage(img) #Get a PhotoImage to pass to our Frame
     tbk.addImage(imagetk) #Image added
     root.update()

#TCP client
try:
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error, msg:
  sys.stderr.write("[ERROR] %s\n" % msg[1])
  sys.exit(1)
 
try:
  sock.connect((HOST_DRIVE, PORT_DRIVE))
except socket.error, msg:
  sys.stderr.write("[ERROR] %s\n" % msg[1])
  sys.exit(2)

#Установка камеры и начальной скорости
sock.send('H'+str(H_CENTER)+'\n\r') # центр по горизонтали
time.sleep(0.1) 
sock.send('v'+str(V_CENTER)+'\n\r') # центр по вертикали
camera_v = V_CENTER
camera_h = H_CENTER
speed = SPEED_1

#Tkinter
root = Tk()
tbk = StreamViewer(root)

#Pygame инициализация
pygame.init()
window = pygame.display.set_mode(SIZE)
pygame.display.set_caption('Control Center')
#Определяем шрифт
font = pygame.font.Font(None, 16)
#Батарея
pygame.draw.polygon(window,green,[[0,0],[0,15],[70,15],[80,0]],0)
textimg = font.render('Battery:',1,white)
window.blit(textimg, (1,1))
#Информация
pygame.draw.polygon(window,green,[[40,240],[50,225],[260,225],[270,240]],0)
textimg = font.render('Use arrows to control robot. SPACE to stop',1,white)
window.blit(textimg, (55,225))
#Прорисовка элементов интерфейса
pygame.draw.aaline(window,(10,100,100),(10,200),(20,300),2)
#Отрисовка
pygame.display.flip()

#As much space as we need, no more, no less
#we change the root geometry to the size of the streaming jpg #As much space as we need, no more, no less

root.geometry("%dx%d+0+0" % SIZE)
root.resizable(False,False)
'''It's our overrated slideshow viewer .. hehe'''
while True:
     streamfile(tbk,root)
     
     for event in pygame.event.get():
        if event.type == QUIT: # обработка нажатий крестика
            exit_proc()
        if event.type == KEYDOWN: # если была нажата клавиша
            if event.key == K_ESCAPE: # если нажат Escape
                exit_proc()
            #Управление шасси
            if event.key == K_LEFT:
                print "Left"
                sock.send('l-' + speed + '\n\r')
                time.sleep(0.1)
                sock.send('r' + speed + '\n\r')
            if event.key == K_RIGHT:
                print "Right"
                sock.send('l' + speed + '\n\r')
                time.sleep(0.1)
                sock.send('r-' + speed + '\n\r')
            if event.key == K_UP:
                print "Forward"
                sock.send('l' + speed + '\n\r')
                time.sleep(0.1)
                sock.send('r' + speed + '\n\r')
            if event.key == K_DOWN:
                print "Backward"
                sock.send('l-' + speed + '\n\r')
                time.sleep(0.1)
                sock.send('r-' + speed + '\n\r')
            if event.key == K_SPACE:
                print "Stop"
                sock.send('h'+'\n\r')
            #Управление камерой
            if event.key == K_w:
                if camera_v < V_MAX:
                    camera_v += VEL_STEP                    
                    sock.send('v'+str(camera_v)+'\n\r')
                print "Camera vertical up" ##
            if event.key == K_s:
                if camera_v > V_MIN:
                    camera_v -= VEL_STEP
                    sock.send('v'+str(camera_v)+'\n\r')
                print "Camera vertical down" ##
            if event.key == K_a:
                if camera_h >H_MIN:
                    camera_h -= VEL_STEP
                    sock.send('H'+str(camera_h)+'\n\r')
                print "Camera horizontal left"
            if event.key == K_d:
                if camera_h <H_MAX:
                    camera_h += VEL_STEP
                    sock.send('H'+str(camera_h)+'\n\r')
                print "Camera horizontal right"
            #Переключение скоростее
            if event.key == K_1:
                print "1st speed"
                speed = SPEED_1
            if event.key == K_2:
                print "2nd speed"
                speed = SPEED_2
            if event.key == K_3:
                print "3rd speed"
                speed = SPEED_3
    

