# -*- coding: utf-8 -*-
"""
Created on Sat Aug 10 17:55:07 2013

@author: danil
"""
import socket
import sys

HOST = '127.0.0.1'
PORT = 5050
#GET = '/rss.xml'

try:
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error, msg:
  sys.stderr.write("[ERROR] %s\n" % msg[1])
  sys.exit(1)
 
try:
  sock.connect((HOST, PORT))
except socket.error, msg:
  sys.stderr.write("[ERROR] %s\n" % msg[1])
  sys.exit(2)
 
#sock.send("GET %s HTTP/1.0\r\nHost: %s\r\n\r\n" % (GET, HOST))
sock.send('Hi!')
data = sock.recv(1024)
print data

sock.close()
sys.exit(0)